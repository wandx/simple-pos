<?php

return [
    "trans_today" => "Transaction Today",
    "total_product" => "Total Product",
    "revenue" => "Revenue",
    "balance" => "Balance",
    "monthly_sales" => "Sales this month",
    "top_product" => "Top Product",
    "running_out" => "Running Out Product",
    "last_transaction" => "Last Transaction",
    "more_info" => "More Info"
];