<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Baris-baris bahasa untuk autentifikasi
    |--------------------------------------------------------------------------
    |
    | Baris bahasa berikut digunakan selama proses autentifikasi untuk beberapa
    | pesan yang perlu kita tampilkan ke pengguna. Anda bebas untuk memodifikasi
    | baris bahasa sesuai dengan keperluan aplikasi anda.
    |
    */

    'failed'   => 'Identitas tersebut tidak cocok dengan data kami.',
    'throttle' => 'Terlalu banyak usaha masuk. Silahkan coba lagi dalam :seconds detik.',
    'success-register' => 'Sukses, cek email untuk konfirmasi',
    'failed-facebook' => 'Gagal masuk dengan facebook',
    'failed-google' => 'Gagal masik dengan google',
    'suspended' => 'User terkena suspend',
    'non-active' => 'User tidak aktif',
    'remember' => 'Ingat Saya',
    'input-email' => 'Masukan email atau username anda',
    'input-password' => 'Masukan password anda',
    'login' => 'Masuk',
    'logout' => 'Keluar',
    'optional-login' => 'Atau masuk dengan',
    'offer-register' => 'Belum memiliki akun?',
    'register-here' => 'Daftar disini',
    'please-login' => 'Silahkan Masuk',
    'forgot-password' => 'Lupa password?'

];