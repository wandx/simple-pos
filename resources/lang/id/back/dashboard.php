<?php

return [
    "trans_today" => "Transaksi hari ini",
    "total_product" => "Total Produk",
    "revenue" => "Keuntungan",
    "balance" => "Balance",
    "monthly_sales" => "Penjualan bulan ini",
    "top_product" => "Produk teratas",
    "running_out" => "Perlu stok tambahan",
    "last_transaction" => "Transaksi terakhir",
    "more_info" => "Info lebih lanjut"
];