@extends("wandx.master")

@section("contents")
    <div class="error-page">

        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Restricted Access</h3>
            <p>
                You don't have access, please login with proper role or contact admin. <br>
                Thank's.
            </p>
        </div>
        <!-- /.error-content -->
    </div>
@stop