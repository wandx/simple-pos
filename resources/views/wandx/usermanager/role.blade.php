@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Usermanager</a></li>
        <li class="active">Roles</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Usermanager
        <small>
            Role lists
        </small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Role</th>
                    <th>Access</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{$role->name}}</td>
                        <td>
                            @foreach($role->menus as $menu)
                                <span class="label label-warning">{{$menu->name}} &nbsp; <i onclick="deleteAccess({{$menu->id}},'{{$menu->name}}','{{$role->name}}',{{$role->id}})" class="fa fa-close" style="cursor:pointer;"></i></span>
                            @endforeach
                        </td>
                        <td>
                            <button data-toggle="modal" data-target="#add-access" data-id="{{$role->id}}" data-name="{{$role->name}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></button>
                            @if($role->menus->count() == 0)
                                <a href="{{ route("adm.roles.delete",['id'=>$role->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="btn-float">
        <button class="btn btn-primary" data-toggle="modal" data-target="#add-role">
            <i class="fa fa-plus"></i>
        </button>
    </div>
@stop

@section('modals')
    <!-- Modal -->
    <div id="add-access" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            {!! Form::open(['route'=>'adm.update_menu']) !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Access</h4>
                </div>
                <div class="modal-body clearfix">
                    @foreach(getAllMenu() as $menu)
                        <div class="col-sm-4">
                            <label for="{{ $menu->slug }}" class="checkbox-inline">
                                <input type="checkbox" name="menus[]" value="{{ $menu->id }}" id="{{ $menu->slug }}"> {{ $menu->name }}
                            </label>
                        </div>
                    @endforeach
                    {{--{!! Form::select('menus[]',allMenuData()->pluck('name','id'),null,['class'=>'form-control','required','multiple','id'=>'menu-sel']) !!}--}}
                    <input type="hidden" name="role_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>

    <!-- Modal -->
    <div id="add-role" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            {!! Form::open(['route'=>'adm.roles.store']) !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Role</h4>
                </div>
                <div class="modal-body">
                    <label for="name">Name</label>
                    <input name="name" type="text" class="form-control" id="name">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop

@section('scripts')
    <script src="/wandx/js/usermanager/role.js"></script>
@stop