@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Report</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Report
        <small>Make transaction report</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <form action="" method="get">
            <div class="well clearfix">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="form-group">
                        <label for="">Start Date</label>
                        <div class="input-group" id="start-date">
                            <input name="start-date" type="text" class="form-control" required>
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">End Date</label>
                        <div class="input-group" id="end-date">
                            <input name="end-date" type="text" class="form-control" required>
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <br>
                        <div class="btn-group">
                            <button type="submit" name="submit" value="view" class="btn btn-info">View</button>
                        </div>

                    </div>

                </div>
            </div>
            </form>
        </div>
    </div>

    @if($transactions != null)
        <div class="panel">
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            Transaction on {{ \Carbon\Carbon::parse(Request::get("start-date"))->format("d/M/Y") }} - {{ \Carbon\Carbon::parse(Request::get("end-date"))->format("d/M/Y") }}
                        </th>
                        <th colspan="3" class="text-right">
                            <a href="{{ route("report.print",["start-date"=>Request::get("start-date"),"end-date"=>Request::get("end-date")]) }}" class="btn btn-primary">Export</a>
                        </th>
                    </tr>
                    <tr>
                        <th>Invoice</th>
                        <th>Admin</th>
                        <th>Grand Total</th>
                        <th>Created at</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($transactions as $transaction)
                        <tr>
                            <td>#{{ $transaction->invoice }}</td>
                            <td>{{ $transaction->admin->name }}</td>
                            <td>{{ $transaction->grand_total }}</td>
                            <td>{{ $transaction->created_at }}</td>
                        </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="4">No data</td>
                            </tr>
                    @endforelse
                    </tbody>
                </table>
                {!! $transactions->render() !!}
            </div>
        </div>
    @endif

@stop

@section("styles")
    <link rel="stylesheet" href="/wandx/css/bootstrap-datetimepicker.min.css">
@stop

@section("modals")

@stop

@section('scripts')
    <script src="/wandx/js/moment-with-locales.min.js"></script>
    <script src="/wandx/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        $(function(){
            $("#start-date").datetimepicker({
                format:"YYYY-MM-DD"
            });

            $("#end-date").datetimepicker({
                format:"YYYY-MM-DD",
                useCurrent:false
            });

            $("#start-date").on("dp.change", function (e) {
                $('#end-date').data("DateTimePicker").minDate(e.date);
            });
            $("#end-date").on("dp.change", function (e) {
                $('#start-date').data("DateTimePicker").maxDate(e.date);
            });
        })
    </script>
@stop