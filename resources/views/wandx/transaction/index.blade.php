@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Transaction</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Transaction
        <small>List of Transaction</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>Invoice</th>
                    <th>Admin</th>
                    <th>Grand Total</th>
                    <th>Cash</th>
                    <th>Return</th>
                    <th>Created at</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section("modals")

@stop

@section('scripts')
    <script>
        var tb;
        $(function() {
            tb = $('#dtable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('transaction.data') !!}',
                columns: [
                    { data: 'invoice', name: 'invoice' },
                    { data: 'admin.name', name: 'admin.name' },
                    { data: 'grand_total', name: 'grand_total' },
                    { data: 'cash', name: 'cash' },
                    { data: 'return', name: 'return' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action','orderable':false,'searchable':false}
                ],
                order:[
                    [
                        3,  "desc"
                    ]
                ]
            });
        });
    </script>
@stop