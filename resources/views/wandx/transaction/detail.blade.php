@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('transaction.index') }}">Transaction</a></li>
        <li class="active">{{ $transaction->invoice }}</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Invoice {{ $transaction->invoice }}
    </h1>
@stop

@section('contents')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Transaction Detail</h3>
        </div>
        <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Item</th>
                    <th class="text-right">Price</th>
                    <th class="text-right">Qty</th>
                    <th class="text-right">Subtotal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($transaction->transactions as $t)
                    <tr>
                        <td>{{ $t->product_name }}</td>
                        <td class="text-right">{{ $t->product_price_sell }}</td>
                        <td class="text-right">{{ $t->qty }}</td>
                        <td class="text-right">{{ $t->subtotal }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td class="text-right bg-success" colspan="3"><b>Grand Total</b></td>
                    <td class="text-right bg-success"><b>{{ $transaction->grand_total }}</b></td>
                </tr>

                <tr>
                    <td class="text-right bg-success" colspan="3"><b>Cash</b></td>
                    <td class="text-right bg-success"><b>{{ $transaction->cash }}</b></td>
                </tr>

                <tr>
                    <td class="text-right bg-success" colspan="3"><b>Return</b></td>
                    <td class="text-right bg-success"><b>{{ $transaction->return }}</b></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section("modals")

@stop

@section('scripts')

@stop