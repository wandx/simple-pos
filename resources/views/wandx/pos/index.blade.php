@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">POS</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        POS
        <small>Point Of Sale</small>
    </h1>
@stop

@section('contents')
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Items</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-hover" id="dtable">
                        <thead>
                        <tr>
                            <th>CODE</th>
                            <th>NAME</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Carts
                    </h3>
                </div>
                <div class="panel-body" id="carts-container">
                    @include('wandx.partials.cart-inc')
                </div>
            </div>
        </div>
    </div>
@stop

@section("modals")
    <!-- Modal -->
    <div id="ret" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">PAY</h4>
                </div>
                {!! Form::open(["route"=>"pos.checkout"]) !!}
                <div class="modal-body">
                    <div class="well text-center">
                        <h4>TOTAL</h4>
                        <b style="font-weight: 800;font-size: 1.8em;" id="tot">100000</b>

                        <hr>

                        <h4>RETURN</h4>
                        <b style="font-weight: 800;font-size: 1.8em;" id="return">0</b>
                    </div>

                    <div class="form-group">
                        <input type="hidden" id="tot-val" name="pay">
                        <input type="hidden" id="return-val" name="return">
                        <input type="number" min="0" class="form-control" name="cash" id="cash" placeholder="CASH">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Finish</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

@stop

@section('scripts')
    <script>
        var tb;
        $(function() {
            tb = $('#dtable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('pos.data') !!}',
                columns: [
                    { data: 'code', name: 'code' },
                    { data: 'name', name: 'name' },
                    { data: 'action', name: 'action','orderable':false,'searchable':false}
                ]
            });
        });

        function redraw_carts(){
            $.get("{{ route('pos.redraw_carts') }}",function(data){
                $("#carts-container").html(data);
            })
        }

        function delete_cart(id) {
            $.get("{{ route('pos.delete_cart') }}/"+id,function(data){
                redraw_carts();
            });
        }

        function add_cart(id){
            $.get("{{ route('pos.add_cart') }}/"+id+"/1",function(data){
                redraw_carts();
            });
        }

        function min_cart(id) {
            $.get("{{ route('pos.add_cart') }}/"+id+"/-1",function(data){
                redraw_carts();
            });
        }

        function clear_carts() {
            $.get("{{ route('pos.clear_carts') }}",function(data){
                redraw_carts();
            });
        }

        $(document).on("show.bs.modal","#ret",function(e){
            var trig = $(e.relatedTarget),
                modal = $(this),
                total = trig.data("total");

            modal.find("#tot").html(total);
            modal.find("#tot-val").val(total);
        });

        $(document).on("keyup","#cash",function(e){
            var tot = $("#tot-val").val();
            var cash = $(this).val();

            if(cash === "" || cash === "0"){
                $("#return").html(tot);
                $("#return-val").val(tot);
            }else{
                tot = parseInt(tot);
                cash = cash === "" ? 0:parseInt(cash);

                var total = cash - tot;

                $("#return").html(total);
                $("#return-val").val(total);
            }




        });


    </script>
@stop