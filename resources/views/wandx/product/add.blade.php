@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Product</a></li>
        <li class="active">Add</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Add Product
        <small>Add product</small>
    </h1>
@stop

@section('contents')
    <div class="row">
        {!! Form::open() !!}
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Product Info</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="code">Code <span class="text-red">*</span></label>
                        <input type="text" name="code" class="form-control" id="code" required>
                    </div>

                    <div class="form-group">
                        <label for="name">Name <span class="text-red">*</span></label>
                        <input type="text" name="name" class="form-control" id="name" required>
                    </div>

                    <div class="form-group">
                        <label for="price-buy">Price Buy <span class="text-red">*</span></label>
                        <input type="number" min="0" name="price-buy" class="form-control" id="price-buy" required>
                    </div>

                    <div class="form-group">
                        <label for="price-sell">Price Sell <span class="text-red">*</span></label>
                        <input type="number" min="0" name="price-sell" class="form-control" id="price-sell" required>
                    </div>

                    <div class="form-group">
                        <label for="brand">Brand <span class="text-red">*</span></label>
                        {!! Form::select("brand_id",$brands,null,["class"=>"form-control","placeholder"=>"Select Brand","id"=>"brand","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="category">Category <span class="text-red">*</span></label>
                        {!! Form::select("category_id",$categories,null,["class"=>"form-control","placeholder"=>"Select Category","id"=>"category","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        {!! Form::textarea("description",null,["class"=>"form-control","id"=>"description","rows"=>3]) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Stock</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="number" name="amount" min="0" class="form-control" id="amount" value="0">
                    </div>

                    <div class="form-group">
                        <label for="unit-id">Unit</label>
                        {!! Form::select("unit_id",$units,null,["class"=>"form-control"]) !!}
                    </div>
                </div>
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-primary">
                    Save
                </button>
            </div>

        </div>

        {!! Form::close() !!}
    </div>


@stop

@section("modals")

@stop

@section('scripts')

@stop