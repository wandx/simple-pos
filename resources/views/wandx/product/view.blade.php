@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('product.index') }}">Products</a></li>
        <li class="active"><a href="#">View</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        View Product
        <small>{{ $product->name }}</small>
    </h1>
@stop

@section('contents')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Product Info</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">CODE</label>
                    <span class="form-control">{{ $product->code }}</span>
                </div>

                <div class="form-group">
                    <label for="">NAME</label>
                    <span class="form-control">{{ $product->name}}</span>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">BRAND</label>
                    <span class="form-control">{{ $product->brand->name}}</span>
                </div>

                <div class="form-group">
                    <label for="">CATEGORY</label>
                    <span class="form-control">{{ $product->category->name }}</span>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">PRICE BUY</label>
                    <span class="form-control">{{ nf($product->price_buy)}}</span>
                </div>

                <div class="form-group">
                    <label for="">PRICE SELL</label>
                    <span class="form-control">{{ nf($product->price_sell) }}</span>
                </div>
            </div>
            
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">Description</label>
                    <textarea name="" id="" cols="3" rows="5" class="form-control" disabled>{{ $product->description }}</textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                Stock History
            </h3>
        </div>
        <div class="panel-body">
            <div class="jumbotron text-center">
                <h1>
                    {{ nf($product->stocks()->sum("pieces") - $sold) }}
                </h1>
                <span style="border-top: 1px dashed black;">Sold: {{ $sold }}</span>

            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Unit</th>
                    <th>Pieces</th>
                    <th>By</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $stocks =  $product->stocks()->paginate(10); ?>
                @forelse($stocks as $stock)
                    <tr>
                        <td>{{ $stock->created_at }}</td>
                        <td>{{ nf($stock->amount) }}</td>
                        <td>{{ $stock->unit->name }}</td>
                        <td>{{ nf($stock->pieces) }}</td>
                        <td>{{ $stock->admin->name }}</td>
                        <td>
                            -
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="5">No Data</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="row">
                <div class="col-sm-6">
                    {{ $stocks->render() }}
                </div>
                <div class="col-sm-6 text-right">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#add-stock">
                        <i class="fa fa-truck fa-flip-horizontal"></i> Add Stock
                    </button>
                </div>
            </div>



        </div>
    </div>

    <div class="btn-float">
        <button class="btn btn-info" data-toggle="modal" data-target="#edit-product">
            <i class="fa fa-pencil"></i>
        </button>
    </div>
@stop

@section("modals")
    <!-- Modal -->
    <div id="add-stock" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Stock</h4>
                </div>
                {!! Form::open(["route"=>"product.add_stock"]) !!}
                <input type="hidden" name="product_id" value="{{ $product->id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="unit">Unit</label>
                        {!! Form::select("unit_id",$units,null,["class"=>"form-control","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="amount">Amount</label>
                        {!! Form::number("amount",null,["class"=>"form-control","required","min"=>0]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Add</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>


    <!-- Modal -->
    <div id="edit-product" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Product</h4>
                </div>
                {!! Form::model($product) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="code">Code</label>
                        {!! Form::text("code",null,["class"=>"form-control","id"=>"code","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label>
                        {!! Form::text("name",null,["class"=>"form-control","id"=>"name","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="category">Category</label>
                        {!! Form::select("category_id",$categories,null,["class"=>"form-control","id"=>"category","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="brand">Brand</label>
                        {!! Form::select("brand_id",$brands,null,["class"=>"form-control","id"=>"brand","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="price_buy">Price Buy</label>
                        {!! Form::number("price_buy",null,["class"=>"form-control","id"=>"price_buy","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="price_sell">Price Sell</label>
                        {!! Form::number("price_sell",null,["class"=>"form-control","id"=>"price_sell","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="desc">Description</label>
                        {!! Form::textarea("description",null,["class"=>"form-control","id"=>"desc","required"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@stop

@section('scripts')

@stop