@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Products</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Products
        <small>List of products</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>CODE</th>
                    <th>Name</th>
                    <th>Qty</th>
                    <th>Price Buy</th>
                    <th>Price Sell</th>
                    <th>Brand</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="btn-float">
        <button class="btn btn-primary" onclick="window.location='{{ route('product.add') }}'">
            <i class="fa fa-plus"></i>
        </button>
    </div>
@stop

@section("modals")
    <!-- Modal -->
    <div id="add-stock" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Stock</h4>
                </div>
                {!! Form::open(["route"=>"product.add_stock"]) !!}
                <input type="hidden" name="product_id" value="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="unit">Unit</label>
                        {!! Form::select("unit_id",$units,null,["class"=>"form-control","required"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="amount">Amount</label>
                        {!! Form::number("amount",null,["class"=>"form-control","required","min"=>0]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Add</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@stop

@section('scripts')
    <script>
        var tb;
        $(function() {
            tb = $('#dtable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('product.data') !!}',
                columns: [
                    { data: 'code', name: 'code' },
                    { data: 'name', name: 'name' },
                    { data: 'stocks_sum', name: 'stocks_sum', 'orderable':false },
                    { data: 'price_buy', name: 'price_buy' },
                    { data: 'price_sell', name: 'price_sell' },
                    { data: 'brand.name', name: 'brand.name','orderable':false },
                    { data: 'category.name', name: 'category.name','orderable':false },
                    { data: 'action', name: 'action','orderable':false,'searchable':false}
                ],
                order:[
                    [2,"desc"]
                ]
            });
        });

        $(document).on("show.bs.modal","#add-stock",function(e){
            var trig = $(e.relatedTarget),
                modal = $(this),
                id = trig.data("id"),
                name = trig.data("name");

            modal.find(".modal-title").html("Add Stock for "+name);
            modal.find("input[name=product_id]").val(id);
        })
    </script>
@stop