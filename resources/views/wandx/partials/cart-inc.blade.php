<div class="col-sm-12">
    <?php $total=0 ;?>
    @forelse(session("carts") as $cart)
        <div class="row" style="font-weight: 800;">
            <div class="col-sm-3">{{ $cart["name"] }}</div>
            <div class="col-sm-2">{{ nf($cart["price"]) }}</div>
            <div class="col-sm-3">
                <button onclick="min_cart('{{ $cart['id'] }}')" class="btn btn-default btn-xs">
                    <i class="fa fa-minus"></i>
                </button>
                <span class="amount" style="margin:auto 5px">x{{ $cart["qty"] }}</span>
                <button onclick="add_cart('{{ $cart['id'] }}',1)" class="btn btn-default btn-xs">
                    <i class="fa fa-plus"></i>
                </button>

            </div>
            <div class="col-sm-3">{{ nf($cart["subtotal"]) }}</div>
            <?php $total += $cart["subtotal"];?>
            <div class="col-sm-1">
                <button onclick="delete_cart('{{ $cart['id'] }}')" class="btn btn-danger btn-xs">
                    <i class="fa fa-trash"></i>
                </button>
            </div>
        </div>
        <div class="row">
            <hr>
        </div>
        @empty
            <div class="well text-center">No Item</div>
    @endforelse
    @if(session()->has("carts") && count(session("carts")) > 0)
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <b>Total</b>
            </div>
            <div class="col-sm-3">
                <b>{{ nf($total) }}</b>
            </div>
        </div>
        <div class="row" style="margin-top: 30px;">
            <div class="text-right">
                <button onclick="clear_carts()" class="btn btn-warning">Clear</button>
                {{--<a href="{{ route("pos.checkout") }}" class="btn btn-primary">Finish</a>--}}
                <button class="btn btn-primary" data-total="{{ $total }}" data-toggle="modal" data-target="#ret">Checkout</button>
            </div>
        </div>
    @endif

</div>
