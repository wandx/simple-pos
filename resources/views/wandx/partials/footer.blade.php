<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        {{ section("setting.adminfooterright") }}
    </div>
    <!-- Default to the left -->
    {{ section("setting.adminfooterleft") }}
</footer>