<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset(auth('admin')->user()->avatar_img)}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{auth('admin')->user()->name}}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{roleName(session('role_id'))}}</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
                {{--<span class="input-group-btn">--}}
                    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
                    {{--</button>--}}
                  {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            @foreach(menuData() as $menu)
                {{--cek submenu availability--}}
                @if($menu->sub_menus->count() == 0)
                    <li class="{{ (Request::segment(2) == $menu->slug) ? 'active' : '' }}">
                        <a href="/backoffice/{{$menu->slug}}"><i class="{{$menu->icon}}"></i> <span>{{$menu->name}}</span></a>
                    </li>
                @else
                    <li class="treeview {{ (Request::segment(2) == $menu->slug || $menu->slug === 'dashboard') ? 'active' : '' }}">
                        <a href="#">
                            <i class="{{$menu->icon}}"></i> <span>{{$menu->name}}</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            @foreach($menu->sub_menus as $sub_menu)
                                {{--cek sub sub menu availability--}}
                                @if($sub_menu->sub_sub_menus->count() == 0)
                                    <li class="{{ (Request::segment(3) == $sub_menu->slug && Request::segment(2) == $menu->slug) ? 'active' : '' }}"><a href="/backoffice/{{$menu->slug}}/{{$sub_menu->slug}}"><i class="{{$sub_menu->icon}}"></i>{{$sub_menu->name}}</a></li>
                                @else
                                    <li class="treeview {{ (Request::segment(3) == $sub_menu->slug && Request::segment(2) == $menu->slug) ? 'active' : '' }}">
                                        <a href="#">
                                            <i class="{{$sub_menu->icon}}"></i> <span>{{$sub_menu->name}}</span>
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            @foreach($sub_menu->sub_sub_menus as $sub_sub_menu)
                                                <li class="{{ (Request::segment(4) == $sub_sub_menu->slug && Request::segment(3) == $sub_menu->slug) ? 'active' : '' }}"><a href="/backoffice/{{$menu->slug}}/{{$sub_menu->slug}}/{{$sub_sub_menu->slug}}"><i class="{{$sub_sub_menu->icon}}"></i>{{$sub_sub_menu->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endforeach
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>