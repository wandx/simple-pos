@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="active">Balance</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Balance
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <div class="jumbotron text-center clearfix">
                <div class="col-sm-6">
                    <h1>
                        {{ nf($netto + $claimed) }}
                    </h1>
                    <span style="border-top: 1px dashed black;">
                        <b>Revenue</b>
                    </span>
                    <br>
                    @if($netto + $claimed > 0)
                        <a href="{{ route('asset.revenue_claim',['amount'=>$netto+$claimed]) }}" onclick="return confirm('Claim revenue?')" class="btn btn-primary btn-xs">Claim</a>
                        <a href="{{ route('asset.move_to_cash',['amount'=>$netto+$claimed]) }}" onclick="return confirm('Claim revenue?')" class="btn btn-primary btn-xs">To Balance</a>
                    @endif
                </div>
                <div class="col-sm-6">
                    <h1>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#act" data-type="out">
                            <i class="fa fa-minus"></i>
                        </button>
                        {{ nf($cash + $balance) }}
                        <button class="btn btn-success" data-toggle="modal" data-target="#act" data-type="in">
                            <i class="fa fa-plus"></i>
                        </button>
                    </h1>

                    <span style="border-top: 1px dashed black;">
                        <b>Balance</b>
                    </span>
                </div>

            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">History</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Type</th>
                            <th>By</th>
                            <th class="text-right">Amount</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($cash_flows as $cash_flow)
                            <tr class="{{ $cash_flow->type == 'in' ? 'bg-success': ($cash_flow->type == 'revenue_claim'?'bg-warning':'bg-danger') }}">
                                <td>{{ $cash_flow->created_at }}</td>
                                <td>{{ $cash_flow->type }}</td>
                                <td>{{ $cash_flow->admin->name }}</td>
                                <td class="text-right">{{ nf($cash_flow->amount) }}</td>
                                <td>{{ $cash_flow->description }}</td>
                                <td>
                                    <a class="btn btn-danger btn-xs" href="{{ route('asset.remove_record',['id'=>$cash_flow->id]) }}" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center" colspan="6">No data</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="text-right">
                        {!! $cash_flows->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section("modals")
    <!-- Modal -->
    <div id="act" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                {!! Form::open(["route"=>"asset.balance"]) !!}
                <div class="modal-body">
                    <div class="well text-center">
                        <h2>{{ nf($cash + $balance) }}</h2>
                    </div>
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="number" name="amount" min="0" class="form-control" id="amount" required>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" cols="30" rows="5"></textarea>
                    </div>
                    <input type="hidden" name="type" id="type">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@stop

@section('scripts')
    <script>
        $("#act").on("show.bs.modal",function(e){
            var trig = $(e.relatedTarget),
                modal = $(this),
                type = trig.data("type");

            modal.find("#type").val(type);

            if(type === "in"){
                modal.find(".modal-title").html("Add Balance");
            }else{
                modal.find(".modal-title").html("Reduce Balance");
            }
        })
    </script>
@stop