@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Setting</a></li>
        <li class="active">Print</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Print
        <small>Print setting</small>
    </h1>
@stop

@section('contents')
    <div class="row">
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="merchant-name">Merchant Name</label>
                        <input type="text" id="merchant-name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="merchant-address">Merchant Address</label>
                        <input type="text" id="merchant-address" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="notes">Notes</label>
                        <input type="text" id="notes" class="form-control">
                    </div>


                </div>
            </div>
        </div>
    </div>

@stop

@section("modals")

@stop

@section('scripts')

@stop