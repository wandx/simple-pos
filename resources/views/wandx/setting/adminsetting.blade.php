@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Setting</a></li>
        <li class="active">Admin</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Setting Admin
        <small>Setting Admin page</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <div class="col-sm-7 col-sm-offset-3">
                {!! Form::open(['files'=>true]) !!}
                <div class="form-group">
                    <label for="title">Title</label>
                    {!! Form::text("title",section("setting.admintitle"),['class'=>'form-control','id'=>'title','required']) !!}
                </div>

                <div class="form-group">
                    <label for="sidebar-label">Sidebar Label</label>
                    {!! Form::text("sidebar-label",section("setting.adminlabel"),['class'=>'form-control','id'=>'sidebar-label','required']) !!}
                </div>

                <div class="form-group">
                    <?php
                        $skins = [
                            "skin-blue" => "Blue",
                            "skin-black" => "Black",
                            "skin-purple" => "Purple",
                            "skin-yellow" => "Yellow",
                            "skin-red" => "Red",
                            "skin-green" => "Green",
                        ];
                    ?>
                    <label for="skin">Skin</label>
                    {{ Form::select("skin",$skins,section("setting.adminskin"),["class"=>"form-control"]) }}
                </div>

                <div class="form-group">
                    <label for="footer-left">Footer Left</label>
                    {!! Form::text("footer-left",section('setting.adminfooterleft'),['class'=>'form-control','id'=>'footer-left']) !!}
                </div>

                <div class="form-group">
                    <label for="footer-right">Footer Right</label>
                    {!! Form::text("footer-right",section('setting.adminfooterright'),['class'=>'form-control','id'=>'footer-right']) !!}
                </div>

                <div class="form-group">
                    <label for="login-bg">
                        Login Background
                        @if(section("setting.adminloginbg") != "")
                            &nbsp;
                            <small>
                                <a href="{{ asset(section('setting.adminloginbg')) }}" target="_blank">view</a>
                                |
                                <a href="{{ route('setting.admin-setting.rm-lbg') }}" onclick="return confirm('Remove login background?')">remove</a>
                            </small>
                        @endif
                    </label>
                    {!! Form::file('login-bg',['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@stop

@section("modals")

@stop

@section('scripts')

@stop