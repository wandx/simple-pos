@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Misc</a></li>
        <li class="active">Brands</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Brands
        <small>List of brands</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="btn-float">
        <button class="btn btn-primary" data-toggle="modal" data-target="#add-brand">
            <i class="fa fa-plus"></i>
        </button>
    </div>
@stop

@section("modals")
    <!-- Modal -->
    <div id="add-brand" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Brand</h4>
                </div>
                <form id="f-add">
                {!! csrf_field() !!}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="edit-brand" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Brand</h4>
                </div>
                <form id="f-edit">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@stop

@section('scripts')
    <script>
        var tb;
        $(function() {
            tb = $('#dtable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('misc.brands.data') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    { data: 'action', name: 'action','orderable':false,'searchable':false}
                ]
            });
        });

        $("#f-add").submit(function(){
            $.ajax({
                url: "{{ route('misc.brands.store') }}",
                data: new FormData(this),
                processData: false,
                contentType:false,
                dataType: "text",
                type: "post",
                success: function(resp){
                    $(document).find("input[name=_token]").val(resp);
                    $("#add-brand").modal("hide");
                    $(this).trigger("reset");
                }
            });
        });

        // edit
        $(document).on("show.bs.modal","#edit-brand",function(e){
            var modal = $(this),
                trig = $(e.relatedTarget),
                id = trig.data("id"),
                name = trig.data("name");

            modal.find("#name").val(name);
            modal.find("input[name=id]").val(id);
        });

        $("#f-edit").submit(function(){
            $.ajax({
                url: "{{ route('misc.brands.update') }}",
                data: new FormData(this),
                processData: false,
                contentType:false,
                dataType: "text",
                type: "post",
                success: function(resp){
                    $(document).find("input[name=_token]").val(resp);
                    $("#edit-brand").modal("hide");
                    $(this).trigger("reset");
                }
            });
        });

        // del
        $(document).on("click",".btn-del",function(e){
            var btn = $(this);
            $.get("{{ route('misc.brands.destroy') }}/"+btn.data("id"),function(data){
                tb.ajax.reload();
            })
        });

        $("#add-brand,#edit-brand").on("hidden.bs.modal",function(){
            tb.ajax.reload();
        })
    </script>
@stop