var addAccess = $('#add-access');
function deleteAccess(id,menu,role,role_id){
    var co = confirm('Remove '+menu+' access from '+role+' ?');
    if(co){
        $.get('/backoffice/usermanager/roles/remove_access/'+id+'/'+role_id,function (data) {
            location.reload();
        })
    }
}

addAccess.on('show.bs.modal',function(e){
    var modal = $(this),
        trig = $(e.relatedTarget),
        id = trig.data('id'),
        name = trig.data('name');

    modal.find('.modal-title').html('Add access to '+name);
    modal.find('input[name="role_id"]').val(id);
    var menuIds = $.getJSON('/backoffice/usermanager/roles/menu_ids/'+id,function(resp){
        resp.map(function(e){
            // $('select#menu-sel option[value="'+e+'"]').prop('selected',true);
            $('input[type=checkbox][value="'+e+'"]').prop('checked',true);
        })
    })
});

addAccess.on('hide.bs.modal',function(){
    $(this).find('form').trigger('reset');
});