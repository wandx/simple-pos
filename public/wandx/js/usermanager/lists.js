$(function(){
    var validator = $('#add-user-form').validate({
        highlight:function(element,eClass,vClass){
            $(element).closest('.form-group').addClass(eClass).removeClass(vClass);
        },
        unhighlight:function(element,eClass,vClass){
            $(element).closest('.form-group').addClass(vClass).removeClass(eClass);
        },
        errorClass:'has-error',
    });

    $('#add-user').on('hide.bs.modal',function(){
        elem = $('#add-user-form');
        elem.trigger('reset');
        var validator = elem.validate();
        validator.resetForm();
    });


    var validator2 = $('#edit-user-form').validate({
        highlight:function(element,eClass,vClass){
            $(element).closest('.form-group').addClass(eClass).removeClass(vClass);
        },
        unhighlight:function(element,eClass,vClass){
            $(element).closest('.form-group').addClass(vClass).removeClass(eClass);
        },
        errorClass:'has-error',
    });

    $('#edit-user').on('hide.bs.modal',function(){
        elem = $('#edit-user-form');
        elem.trigger('reset');
        var validator = elem.validate();
        validator.resetForm();
    });
});

$(document).on('show.bs.modal','#edit-user',function(e){
     var trig = $(e.relatedTarget);
     var modal = $(this);
     var id = trig.data('id');
     var name = trig.data('name');

     modal.find('.modal-title').html(name);

     modal.find('form#edit-user-form').attr('action','/backoffice/usermanager/update/'+id);
     $.getJSON('/backoffice/usermanager/userdata/'+id,function(data){
         if(data.avatar_img !== null){
             modal.find('img#img-prev2').attr('src',data.avatar_img).removeClass('hidden').prev().addClass('hidden');
             modal.find('input#name').val(data.name);
             modal.find('input#username').val(data.username);
             modal.find('input#email').val(data.email);

             // $.each(data.roles)
             data.roles.map(function(e){
                 $('select#roles option[value="'+ e.id +'"]').prop('selected',true);
                 console.log(e);
             })
         }

         console.log(data);
     })
});

$(document).on('hide.bs.modal',function(e){
    var modal = $(this);
    modal.find('img#img-prev').addClass('hidden').prev().removeClass('hidden');
    $('#edit-user-form').trigger('reset');
});

function deleteUser(id) {
    var co = confirm('Delete Admin ?');

    if(co){
        $.get('/backoffice/usermanager/destroy/'+id,function(data){
            location.reload();
        });
    }
}

