<?php

namespace App\Http\Controllers\Wandx\Dashboard;

use App\Models\CashFlow;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardCont extends Controller
{
    public function index(){
        $data = [
            "transaction" => $this->transaction_today(),
            "product" => $this->total_product(),
            "revenue" => $this->revenue(),
            "balance" => $this->balance(),
            "top_products" => $this->top_product(),
            "running_out_products" => $this->running_out_product(),
            "last_transactions" => $this->last_transaction()
        ];
        return view('wandx.dashboard.index',$data);
    }

    private function transaction_today(){
        $t = new TransactionDetail();
        $t = $t->newQuery();
        $t->whereBetween("created_at",[
            Carbon::today()->format("Y-m-d H:i:s"),
            Carbon::now()->format("Y-m-d H:i:s")
        ]);

        return $t->count();
    }

    private function total_product(){
        $p = new Product();
        $p = $p->newQuery();
        return $p->count();
    }

    private function revenue(){
        $cf = new CashFlow();
        $t = new Transaction();

        $netto = $t->newQuery()->sum("revenue");
        $claimed = $cf->newQuery()->whereType("revenue_claim")->sum("amount");

        return $netto+$claimed;
    }

    private function balance(){
        $td = new TransactionDetail();
        $cf = new CashFlow();

        $cash = $td->newQuery()->sum("grand_total");
        $balance = $cf->newQuery()->sum("amount");

        return $cash+$balance;
    }

    private function top_product(){
        $p = new Product();

        $p = $p->newQuery();

        $p = $p->take(5)->withCount("transactions")->orderBy("transactions_count","desc")->get();

        return $p;
    }

    private function running_out_product(){
        $p = new Product();

        $p = $p->newQuery();

        $p = $p->join("stocks","stocks.product_id","=","products.id")
            ->selectRaw(
                "products.*,
                        (sum(stocks.pieces) - (
                            select 
                                case 
                                    when sum(transactions.qty) is null then '0' 
                                    else sum(transactions.qty) 
                                end  
                            from transactions 
                            where transactions.product_id = products.id) 
                        ) as stocks_sum"
            )
            ->groupBy("products.id")
            ->whereRaw("stocks.product_id = products.id")
            ->having("stocks_sum","<",10)
            ->orderBy("stocks_sum","asc")
            ->take(5)
            ->get();

        return $p;
    }

    private function last_transaction(){
        $td = new TransactionDetail();

        return $td->newQuery()->take(5)->latest()->get();
    }
}
