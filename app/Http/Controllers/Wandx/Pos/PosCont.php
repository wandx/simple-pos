<?php

namespace App\Http\Controllers\Wandx\Pos;

use App\Models\Product;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Form;

class PosCont extends Controller
{
    private $carts = [];

    public function index(){
        session()->put("carts",[]);
        return view("wandx.pos.index");
    }

    /***
     * @param Product $product
     * @return mixed
     * @throws \Exception
     */
    public function data(Product $product){
        return Datatables::of(
                    $product->newQuery()
                            ->select("products.*",\DB::raw("SUM(stocks.pieces) as total_stock"))
                            ->join("stocks","stocks.product_id","=","products.id")
                            // ->havingRaw("(total_stock - (select sum(qty) from transactions where product_id = products.id)) > 0")
                            ->groupBy("products.id")
                )
                ->addColumn("action",function($model){
                    return Form::button("<i class='fa fa-plus'></i>",["class"=>"btn btn-primary btn-xs","onclick"=>"add_cart('".$model->id."')"]);
                })
                ->make(true);
    }

    public function delete_cart($id){
        if(session()->has("carts")){
            $container = [];
            foreach (session("carts") as $cart){
                if($cart["id"] != $id){
                    $container[] = $cart;
                }
            }

            session()->put("carts",$container);
        }
    }

    public function add_cart($id,$amount,Product $product){
        $p = $product->find($id);
        $carts = session()->has("carts") ? session("carts"):session("carts",[]);

        // check trough carts , is $id already registered?
        if(count($carts) > 0){
            $container = [];
            $id_exist = 0;
            foreach ($carts as $k=>$c){
                if($c["id"] == $id){
                    $id_exist = 1;
                    $c["qty"] += $amount;
                    $c["subtotal"] = $c["qty"]*$c["price"];
                }

                if($c["qty"] > 0){
                    $container[] = $c;
                }

            }
            session()->put("carts",$container);

            if($id_exist == 0){
                session()->push("carts",$this->cart_obj($id,$p->name,(int) $amount,$p->price_sell,$p->price_sell*$amount));
            }

        }else{
            session()->push("carts",$this->cart_obj($id,$p->name,(int) $amount,$p->price_sell,$p->price_sell*$amount));
        }

//        session()->forget("carts")''
//        return session("carts");

    }

    private function cart_obj($id,$name,$qty,$price,$subtotal){
        return [
            "id" => $id,
            "name" => $name,
            "qty" => $qty,
            "price" => $price,
            "subtotal" => $subtotal
        ];
    }

    private function append_cart(array $cart){
        $this->carts[$cart["id"]] = $cart;
        return $this->carts;
    }

    public function redraw_carts(){
        return view("wandx.partials.cart-inc");
    }

    public function clear_carts(){
        session()->put("carts",[]);
    }

    public function checkout(Request $request,Product $product,TransactionDetail $detail){
//        return $request->all();
        if(session()->has("carts") && count(session("carts")) > 0){
            $invoice = $detail->newQuery()->create([
                "invoice" => Carbon::now()->format("dmYHis")."-tr",
                "admin_id" => auth("admin")->user()->id,
                "admin_name" => auth("admin")->user()->name,
                "grand_total" => $this->grand_total(),
                "cash" => $request->input("cash"),
                "return" => $request->input("return")
            ]);

            foreach (session("carts") as $cart){
                $p = $product->newQuery()->find($cart["id"]);
                $invoice->transactions()->create([
                    "product_id" => $cart["id"],
                    "product_name" => $cart["name"],
                    "product_price_buy" => $p->price_buy,
                    "product_price_sell" => $p->price_sell,
                    "qty" => $cart["qty"],
                    "subtotal" => $cart["subtotal"],
                    "revenue" => ($p->price_sell - $p->price_buy)*$cart["qty"]
                ]);
            }
            session()->put("carts",[]);
        }

        return redirect()->back();
    }

    private function grand_total(){
        $total = 0;

        if(session()->has("carts") && count(session("carts")) > 0){
            foreach (session("carts") as $cart){
                $total += $cart["subtotal"];
            }
        }

        return $total;
    }
}
