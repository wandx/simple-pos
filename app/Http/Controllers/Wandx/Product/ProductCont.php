<?php

namespace App\Http\Controllers\Wandx\Product;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ProductCont extends Controller
{
    public function index(Unit $unit){
        $data = [
            "units" => $unit->newQuery()->pluck("name","id")
        ];
        return view("wandx.product.index",$data);
    }

    public function view_product($id,Product $product,Unit $unit,Transaction $transaction,Category $category,Brand $brand){
        $data = [
            "product" => $product->newQuery()->find($id),
            "units" => $unit->newQuery()->pluck("name","id"),
            "sold" => $transaction->newQuery()->whereProductId($id)->sum("qty"),
            "categories" => $category->newQuery()->pluck("name","id"),
            "brands" => $brand->newQuery()->pluck("name","id")
        ];
        return view("wandx.product.view",$data);
    }
    /***
     * @param Product $product
     * @return mixed
     * @throws \Exception
     */
    public function data(Product $product){
        return Datatables::of(
            $product->newQuery()->with("stocks","brand","category")
//                    $p = $product->join("stocks","stocks.product_id","=","products.id")
//                    ->selectRaw(
//                        "products.*,
//                            (sum(stocks.pieces) - (
//                                select
//                                    case
//                                        when sum(transactions.qty) is null then '0'
//                                        else sum(transactions.qty)
//                                    end
//                                from transactions
//                                where transactions.product_id = products.id)
//                            ) as stocks_sum"
//                    )
//                    ->groupBy("products.id")
//                    ->whereRaw("stocks.product_id = products.id")
//                    ->with("category","brand")
                )
                ->addColumn("stocks_sum",function($model){
                    return $model->stocks()->sum("pieces") - sold_qty($model->id);
                })
                ->addColumn("action",function($model){
                    $act = "";

                    $act .= \Form::button("<i class='fa fa-truck fa-flip-horizontal'></i>",[
                        "class"=>"btn btn-primary btn-xs",
                        "data-toggle" => "modal",
                        "data-target" => "#add-stock",
                        "data-id" => $model->id,
                        "data-name" => $model->name
                    ]);
                    $act .= "<a href='".route('product.view',['id'=>$model->id])."' class='btn btn-info btn-xs'><i class='fa fa-eye'></i></a>";

                    return $act;
                })
                ->make(true);
    }

    public function add(Category $category,Brand $brand,Unit $unit){
        $data = [
            "categories" =>$category->newQuery()->pluck("name","id"),
            "brands" => $brand->newQuery()->pluck("name","id"),
            "units" => $unit->newQuery()->pluck("name","id")
        ];
        return view("wandx.product.add",$data);
    }

    public function store(Request $request,Product $product){
        $data = [
            "code" => $request->input("code"),
            "name" => $request->input("name"),
            "category_id" => $request->input("category_id"),
            "brand_id" => $request->input("brand_id"),
            "description" => $request->input("description"),
            "price_buy" => $request->input("price-buy"),
            "price_sell" => $request->input("price-sell")
        ];

        $store = $product->newQuery()->create($data);

        if($request->has("amount")){
            $store->stocks()->create([
                "unit_id" => $request->input("unit_id"),
                "amount" => $request->input("amount"),
                "stock_activity_id" => 1
            ]);
        }

        return redirect()->route("product.index");
    }

    public function update($id,Request $request,Product $product){
        $data = [
            "code" => $request->input("code"),
            "name" => $request->input("name"),
            "category_id" => $request->input("category_id"),
            "brand_id" => $request->input("brand_id"),
            "description" => $request->input("description"),
            "price_buy" => $request->input("price_buy"),
            "price_sell" => $request->input("price_sell")
        ];

        $product->newQuery()->find($id)->update($data);
        return redirect()->back();
    }

    public function add_stock(Request $request,Product $product){
        $data = [
            "unit_id" => $request->input("unit_id"),
            "amount" => $request->input("amount"),
            "stock_activity_id" => 1
        ];

        $product->newQuery()->find($request->input("product_id"))->stocks()->create($data);
        return redirect()->back();
    }
}
