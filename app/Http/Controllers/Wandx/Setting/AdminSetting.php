<?php

namespace App\Http\Controllers\Wandx\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class AdminSetting extends Controller
{
    public function index(){
        return view('wandx.setting.adminsetting');
    }

    public function save_setting(Request $request){

        if($request->has("title")){
            update_section("setting.admintitle",$request->input('title'));
        }

        if($request->has("sidebar-label")){
            update_section("setting.adminlabel",$request->input('sidebar-label'));
        }

        if($request->has("skin")){
            update_section("setting.adminskin",$request->input('skin'));
        }

        if($request->has("footer-left")){
            update_section("setting.adminfooterleft",$request->input('footer-left'));
        }

        if($request->has("footer-right")){
            update_section("setting.adminfooterright",$request->input('footer-right'));
        }

        if($request->hasFile('login-bg')){
            $name = str_random(10).".jpg";
            $path = "uploads/lbg/";
            $filename = $path.$name;

            Image::make($request->file("login-bg"))->save($filename);
            update_section("setting.adminloginbg",$filename);
        }

        return redirect()->back();
    }

    public function removeLoginBg(){
        try{
            unlink(section("setting.adminloginbg"));
        }catch (\Exception $e){}

        update_section("setting.adminloginbg","");
        return redirect()->back();
    }
}
