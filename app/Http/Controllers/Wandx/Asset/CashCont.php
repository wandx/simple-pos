<?php

namespace App\Http\Controllers\Wandx\Asset;

use App\Models\CashFlow;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CashCont extends Controller
{
    public function index(TransactionDetail $detail,CashFlow $cashFlow,Transaction $transaction){
        $data = [
            "cash" => $detail->newQuery()->sum("grand_total"),
            "netto" => $transaction->newQuery()->sum("revenue"),
            "claimed" => $cashFlow->newQuery()->whereType("revenue_claim")->sum("amount"),
            "balance" => $cashFlow->newQuery()->sum("amount"),
            "cash_flows" => $cashFlow->newQuery()->latest()->paginate(10)
        ];

        return view("wandx.asset.cash",$data);
    }

    public function balance(Request $request,CashFlow $cashFlow){
//        return (int) $request->input("amount")*-1;
        $cashFlow->newQuery()->create([
            "type" => $request->input("type"),
            "amount" => $request->input("type") == "in" ? $request->input("amount"):$request->input("amount")*-1,
            "admin_id" => auth("admin")->user()->id,
            "description" => $request->input("description")
        ]);
        return redirect()->back();
    }

    public function revenue_claim($amount,CashFlow $cashFlow){
        $cashFlow->newQuery()->create([
            "type" => "revenue_claim",
            "amount" => $amount*-1,
            "admin_id" => auth("admin")->user()->id,
            "description" => "revenue claim"
        ]);

        return redirect()->back();
    }

    public function move_to_cash($amount,CashFlow $cashFlow){
        $cashFlow->newQuery()->create([
            "type" => "revenue_claim",
            "amount" => $amount*-1,
            "admin_id" => auth("admin")->user()->id,
            "description" => "revenue claim"
        ]);

        $cashFlow->newQuery()->create([
            "type" => "in",
            "amount" => $amount,
            "admin_id" => auth("admin")->user()->id,
            "description" => "move to balance"
        ]);

        return redirect()->back();

    }

    public function remove_record($id,CashFlow $cashFlow){
        $cashFlow->newQuery()->find($id)->delete();
        return redirect()->back();
    }
}
