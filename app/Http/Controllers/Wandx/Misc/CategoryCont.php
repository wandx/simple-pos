<?php

namespace App\Http\Controllers\Wandx\Misc;

use App\Models\Category;
use Form;
use Html;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class CategoryCont extends Controller
{
    public function index(){
        return view("wandx.misc.category");
    }
    /***
     * @param Category $category
     * @return mixed
     * @throws \Exception
     */
    public function data(Category $category){
        return Datatables::of($category->newQuery()->with("products"))
            ->addColumn("action",function($model){
                $act = "";
                $act .= Form::button("<i class='fa fa-pencil'></i>",[
                    "class"=>"btn btn-info btn-xs",
                    "data-id"=>$model->id,
                    "data-name"=>$model->name,
                    "data-toggle"=>"modal",
                    "data-target"=>"#edit-brand"
                ]);
                $act .= Html::nbsp();
                if($model->products->count() == 0){
                    $act .= Form::button("<i class='fa fa-trash'></i>",[
                        "class"=>"btn btn-danger btn-xs btn-del",
                        "data-id"=>$model->id,
                        "data-name"=>$model->name
                    ]);
                }

                return $act;
            })
            ->make(true);
    }

    public function store(Request $request,Category $category){
        $data = [
            "name" => $request->input("name")
        ];

        $category->newQuery()->create($data);
        return csrf_token();
    }

    public function update(Request $request,Category $category){
        $data = [
            "name" => $request->input("name")
        ];

        $category->newQuery()->find($request->input("id"))->update($data);
        return csrf_token();
    }

    public function destroy($id,Category $category){
        $b = $category->newQuery()->find($id);

        if($b->products->count() == 0){
            $b->delete();
        }
    }
}
