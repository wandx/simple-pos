<?php

namespace App\Http\Controllers\Wandx\Misc;

use App\Models\Unit;
use Form;
use Html;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class UnitCont extends Controller
{
    public function index(){
        return view("wandx.misc.unit");
    }
    /***
     * @param Unit $unit
     * @return mixed
     * @throws \Exception
     */
    public function data(Unit $unit){
        return Datatables::of($unit->newQuery()->with("stocks"))
            ->addColumn("action",function($model){
                $act = "";
                $act .= Form::button("<i class='fa fa-pencil'></i>",[
                    "class"=>"btn btn-info btn-xs",
                    "data-id"=>$model->id,
                    "data-name"=>$model->name,
                    "data-amount" => $model->amount,
                    "data-toggle"=>"modal",
                    "data-target"=>"#edit-brand"
                ]);
                $act .= Html::nbsp();
                if($model->stocks()->count() == 0){
                    $act .= Form::button("<i class='fa fa-trash'></i>",[
                        "class"=>"btn btn-danger btn-xs btn-del",
                        "data-id"=>$model->id,
                        "data-name"=>$model->name
                    ]);
                }

                return $act;
            })
            ->make(true);
    }

    public function store(Request $request,Unit $unit){
        $data = [
            "name" => $request->input("name"),
            "amount" => $request->input("amount")
        ];

        $unit->newQuery()->create($data);
        return csrf_token();
    }

    public function update(Request $request,Unit $unit){
        $data = [
            "name" => $request->input("name"),
            "amount" => $request->input("amount")
        ];

        $unit->newQuery()->find($request->input("id"))->update($data);
        return csrf_token();
    }

    public function destroy($id,Unit $unit){
        $b = $unit->newQuery()->find($id);

        if($b->stocks()->count() == 0){
            $b->delete();
        }
    }
}
