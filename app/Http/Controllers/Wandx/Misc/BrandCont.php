<?php

namespace App\Http\Controllers\Wandx\Misc;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Form;
use Html;

class BrandCont extends Controller
{
    public function index(){
        return view("wandx.misc.brand");
    }
    /***
     * @param Brand $brand
     * @return mixed
     * @throws \Exception
     */
    public function data(Brand $brand){
        return Datatables::of($brand->newQuery()->with("products"))
                ->addColumn("action",function($model){
                    $act = "";
                    $act .= Form::button("<i class='fa fa-pencil'></i>",[
                        "class"=>"btn btn-info btn-xs",
                        "data-id"=>$model->id,
                        "data-name"=>$model->name,
                        "data-toggle"=>"modal",
                        "data-target"=>"#edit-brand"
                    ]);
                    $act .= Html::nbsp();
                    if($model->products()->count() == 0){
                        $act .= Form::button("<i class='fa fa-trash'></i>",[
                            "class"=>"btn btn-danger btn-xs btn-del",
                            "data-id"=>$model->id,
                            "data-name"=>$model->name
                        ]);
                    }

                    return $act;
                })
                ->make(true);
    }

    public function store(Request $request,Brand $brand){
        $data = [
            "name" => $request->input("name")
        ];

        $brand->newQuery()->create($data);
        return csrf_token();
    }

    public function update(Request $request,Brand $brand){
        $data = [
            "name" => $request->input("name")
        ];

        $brand->newQuery()->find($request->input("id"))->update($data);
        return csrf_token();
    }

    public function destroy($id,Brand $brand){
        $b = $brand->newQuery()->find($id);

        if($b->products()->count() == 0){
            $b->delete();
        }
    }
}
