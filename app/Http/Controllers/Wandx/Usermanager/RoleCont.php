<?php

namespace App\Http\Controllers\Wandx\Usermanager;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleCont extends Controller
{
    public function index(Role $role){
        $data = [
            'roles' => $role->newQuery()->get()
        ];

        return view('wandx.usermanager.role',$data);

    }

    public function menuIds($role_id,Role $role){
        $cek = $role->newQuery()->find($role_id);

        if($cek->menus->count() > 0){
            return $cek->menus->pluck('id');
        }

        return [];
    }

    public function updateMenu(Request $request,Role $role){
        $role_id = $request->input('role_id');
        $role->newQuery()->find($role_id)->menus()->sync($request->input('menus'));
        return redirect()->back()->with(['success'=>'Saved']);
    }

    public function removeAccess($id,$role_id,Role $role){
        $role->newQuery()->find($role_id)->menus()->detach($id);
    }

    public function store_role(Request $request,Role $role){
        $role->newQuery()->create(['name'=>$request->input('name')]);
        return redirect()->back();
    }

    public function delete_role($id,Role $role){
        $cek = $role->newQuery()->find($id)->menus->count();

        if($cek == 0){
            $role->newQuery()->find($id)->delete();
        }

        return redirect()->back();
    }
}
