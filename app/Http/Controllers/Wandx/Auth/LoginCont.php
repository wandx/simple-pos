<?php

namespace App\Http\Controllers\Wandx\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginCont extends Controller
{
    public function index(){
        return view('wandx.login');
    }

    public function doLogin(Request $request){
        $validator = app('validator')->make($request->all(),[
            'email' => 'required',
            'password' => 'required|min:7'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->messages());
        }

        $remember = $request->input('remember',0);
        $remember = $remember == 1 ? true:false;

        if(auth('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$remember)){
            return redirect()->route('adm.dashboard');
        }else if(auth('admin')->attempt(['username'=>$request->email,'password'=>$request->password],$remember)){
            return redirect()->route('adm.dashboard');
        }

        return redirect()->back()->withErrors(['failed'=>'Email/password tidak benar']);
    }

    public function roles(){

        if(session('role_selected')){
            return redirect()->route('adm.dashboard');
        }

        $data = [
            'roles' => auth('admin')->user()->roles->pluck('name','id')
        ];
        return view('wandx.select-role',$data);
    }

    public function rolesPost(Request $request){
        session(['role_selected' => true,'role_id' => $request->input('role')]);
        return redirect()->route('adm.dashboard');
    }

    public function logout(){
        auth('admin')->logout();
        session()->forget(['role_selected','role_id']);
        return redirect()->route('adm.login');
    }
}
