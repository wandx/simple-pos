<?php

namespace App\Http\Controllers\Wandx\Transaction;

use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class TransCont extends Controller
{
    public function index(){
        return view("wandx.transaction.index");
    }
    /***
     * @param TransactionDetail $detail
     * @return mixed
     * @throws \Exception
     */
    public function data(TransactionDetail $detail){
        return Datatables::of($detail->newQuery()->with("admin"))
                ->addColumn("action",function($model){
                    return "<a href='".route('transaction.detail',['id'=>$model->id])."' class='btn btn-info btn-xs'><i class='fa fa-eye'></i></a>";
                })
                ->make(true);
    }

    public function detail($id,TransactionDetail $detail){
        $data = [
            "transaction" => $detail->newQuery()->find($id)
        ];

        return view("wandx.transaction.detail",$data);
    }
}
