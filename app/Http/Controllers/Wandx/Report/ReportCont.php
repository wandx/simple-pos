<?php

namespace App\Http\Controllers\Wandx\Report;

use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportCont extends Controller
{
    public function index(Request $request,TransactionDetail $transaction){
        $data = [
            "transactions" => $this->tdata($request,$transaction)
        ];
        return view("wandx.report.index",$data);
    }

    public function print_out(Request $request,TransactionDetail $transaction){
        $transaction = $transaction->newQuery();
        $transaction->whereBetween("transaction_details.created_at",[
            Carbon::parse($request->input("start-date"))->format("Y-m-d H:i:s"),
            Carbon::parse($request->input("end-date"))->format("Y-m-d H:i:s")
        ]);

        $header = [
            "Invoice",
            "Admin",
            "Grand Total",
            "Created at"
        ];

        $foot = [
            "",
            "TOTAL",
            $transaction->sum("grand_total"),
            ""
        ];

        $data = $transaction->select("transaction_details.invoice","admins.name","transaction_details.grand_total","transaction_details.created_at")->join("admins","transaction_details.admin_id","admins.id")->get()->toArray();
        $filename = "Report ".Carbon::parse($request->input("start-date"))->format("d/M/Y")." - ".Carbon::parse($request->input("end-date"))->format("d/M/Y").".csv";
        return $this->a2csv_download($header,$data,$filename,";",$foot);
    }

    private function tdata(Request $request,TransactionDetail $transaction){
        if($request->has("submit")){
            if($request->input("submit") == "view"){
                $transaction = $transaction->newQuery();
                $transaction->whereBetween("created_at",[
                    Carbon::parse($request->input("start-date"))->format("Y-m-d H:i:s"),
                    Carbon::parse($request->input("end-date"))->format("Y-m-d H:i:s")
                ]);
                return $transaction->paginate(10)->appends($request->query());
            }
        }

        return null;
    }

    private function a2csv_download($column_header,array $array, $filename = "export.csv", $delimiter=";",$foot) {
        $f = fopen('php://memory', 'w');
        if(!empty($column_header)){
            fputcsv($f,$column_header,$delimiter);
        }
        foreach ($array as $line) {
            fputcsv($f, $line, $delimiter);
        }
        if(!empty($foot)){
            fputcsv($f,$foot,$delimiter);
        }
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        fpassthru($f);
        fclose($f);
    }
}
