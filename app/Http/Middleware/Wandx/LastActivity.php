<?php

namespace App\Http\Middleware\Wandx;

use Carbon\Carbon;
use Closure;

class LastActivity
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(auth('admin')->check()){
            $data = [
                'activity' => $request->getUri(),
                'time' => Carbon::now(),
                'ip' => $request->ip(),
                'code' => $response->status()
            ];
            auth('admin')->user()->activities()->create($data);
        }

        return $response;
    }
}
