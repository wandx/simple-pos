<?php

// Build menu
function menuData(){
    $menu = new \App\Models\Menu();
    $menu = $menu->newQuery()->whereHas('roles',function($role){
        $role->where('id',session('role_id'));
    })->with('sub_menus.sub_sub_menus')->get();

    return $menu;
}

// get all menu
function allMenuData(){
    $menu = new \App\Models\Menu();
    $menu = $menu->newQuery()->with('sub_menus.sub_sub_menus')->get();

    return $menu;
}

// Get role by id
function roleName($id){
    $role = new \App\Models\Role();
    $role = $role->newQuery()->find($id);

    return $role == null ? "-" : $role->name;
}

// Get last activity (admin)
function lastActivity(){
    if(auth('admin')->check()){
        $ac = new \App\Models\AdminActivity();
        $ac = $ac->newQuery()->whereHas('admin',function($admin){
            $admin->where('id',auth('admin')->user()->id);
        })->orderBy('time','desc')->first();

        return $ac == null ? "no activity yet":$ac->time->format('d M Y, H:i:s');
    }
    return "no activity";
}

function roleData(){
    $role = new \App\Models\Role();
    return $role->newQuery()->pluck('name','id');
}

function section($key){
    return (new \App\Models\Section())->newQuery()->where("key",$key)->first()->value ?? "";
}

function update_section($key,$val){
    (new \App\Models\Section())->where("key",$key)->update(["value"=>$val]);
}

function getAllMenu(){
    $x = (new \App\Models\Menu())->newQuery()->get();
    return $x;
}

function nf($amount){
    return number_format($amount,0,",",".");
}

function sold_qty($pid){
    $val = (new \App\Models\Transaction())->newQuery()->whereProductId($pid)->sum("qty");
    return $val;
}

function transaction_series(){
    $t = new \App\Models\TransactionDetail();

    $t = $t->newQuery()
        ->select(DB::raw("DATE(created_at) as date"),DB::raw("sum(grand_total) as gt"))
        ->whereMonth("created_at","=",\Carbon\Carbon::today()->format("m"))
        ->whereYear("created_at","=",\Carbon\Carbon::today()->format("Y"))
        ->groupBy("date")
        ->orderBy("date","asc")
        ->get();

    $label = [];
    $data = [];
    foreach ($t as $tt){
        $label[] = \Carbon\Carbon::parse($tt->date)->format("d");
    }

    foreach ($t as $ttt){
        $data[] = $ttt->gt;
    }

    return [
        "label" => $label,
        "quantity" => $data
    ];
}