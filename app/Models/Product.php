<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use UuidForKey;

    protected $guarded = ["id"];
    public $incrementing = false;
    protected $appends = ["quantity"];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function stocks(){
        return $this->hasMany(Stock::class);
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }

    public function getQuantityAttribute(){
        $x = $this->stocks()->sum("pieces") - sold_qty($this->id);
        return $x;
    }
}
