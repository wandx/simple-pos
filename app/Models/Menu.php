<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $timestamps = false;

    public function sub_menus(){
        return $this->hasMany(SubMenu::class);
    }

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
}
