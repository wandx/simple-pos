<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];

    public function admins(){
        return $this->belongsToMany(Admin::class);
    }

    public function menus(){
        return $this->belongsToMany(Menu::class);
    }

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        self::creating(function($model){
            $model->slug = Str::slug($model->name);
        });

        self::created(function($model){
            $model->menus()->attach(1);
        });

        self::updating(function($model){
            $model->slug = Str::slug($model->name);
        });
    }
}
