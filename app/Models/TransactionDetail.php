<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
     use UuidForKey;

     protected $guarded = ["id"];
     public $incrementing = false;

     public function transactions(){
         return $this->hasMany(Transaction::class);
     }

     public function admin(){
         return $this->belongsTo(Admin::class);
     }
}
