<?php

namespace App\Console\Commands;

use App\Models\Section;
use Illuminate\Console\Command;

class SectionConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'section:make {key} {value}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'make section';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new Section())->newQuery()->updateOrCreate(['key'=>$this->argument("key")],[
            'key' => $this->argument("key"),
            'value' => $this->argument("value")
        ]);
        print "OK";
    }
}
