<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;

class TestPrinter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'printer:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /***
     * @throws \Exception
     */
    public function handle()
    {
        $connector = new FilePrintConnector("/dev/usb/lp0");
        $printer = new Printer($connector);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("Hello World!\n");
        $printer->text("-------------------------------\n");
        $printer->text("sgih uewghwiuegh weughw ughwrugh wrugh wruoghwrugh wruogh wruow");
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("Hello World!\n");
        $printer->text("\n");
        $printer->text("\n");
        $printer->cut();
        $printer->close();

    }
}
