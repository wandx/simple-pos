<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid("product_id")->nullable();
            $table->string("product_name");
            $table->double("product_price_buy",11,2);
            $table->double("product_price_sell",11,2);
            $table->double("qty",11,2);
            $table->double("subtotal",11,2);
            $table->decimal("revenue",11,2);
            $table->uuid("transaction_detail_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
