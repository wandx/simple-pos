<?php

use Illuminate\Database\Seeder;

class MenuSeederForce extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        \Illuminate\Database\Eloquent\Model::unguard();
        DB::table('menus')->truncate();
        DB::table('sub_menus')->truncate();
        DB::table('sub_sub_menus')->truncate();

        DB::table('menus')->insert([
            [
                'id' => 1,
                'name' => 'Dashboard',
                'slug' => 'dashboard',
                'icon' => 'fa fa-dashboard'
            ],
            [
                'id' => 2,
                'name' => 'User Manager',
                'slug' => 'usermanager',
                'icon' => 'fa fa-users'
            ],
            [
                'id' => 3,
                'name' => 'Product',
                'slug' => 'product',
                'icon' => 'fa fa-shopping-basket'
            ],
            [
                'id' => 4,
                'name' => 'Misc',
                'slug' => 'misc',
                'icon' => 'fa fa-archive'
            ],
            [
                'id' => 5,
                'name' => 'Point Of Sale',
                'slug' => 'pos',
                'icon' => 'fa fa-shopping-cart'
            ],
            [
                'id' => 6,
                'name' => 'Transaction',
                'slug' => 'transaction',
                'icon' => 'fa fa-exchange'
            ],
            [
                'id' => 7,
                'name' => 'Balance',
                'slug' => 'asset',
                'icon' => 'fa fa-money'
            ],
            [
                'id' => 8,
                'name' => 'Report',
                'slug' => 'report',
                'icon' => 'fa fa-bug'
            ],
            [
                'id' => 100,
                'name' => 'Setting',
                'slug' => 'setting',
                'icon' => 'fa fa-cog'
            ]
        ]);

        DB::table('sub_menus')->insert([
            [
                'name' => 'Lists',
                'slug' => 'lists',
                'icon' => 'fa fa-tasks',
                'menu_id' => 2
            ],
            [
                'name' => 'Roles',
                'slug' => 'roles',
                'icon' => 'fa fa-tasks',
                'menu_id' => 2
            ],
            [
                'name' => 'Brands',
                'slug' => 'brands',
                'icon' => 'fa fa-building',
                'menu_id' => 4
            ],
            [
                'name' => 'Categories',
                'slug' => 'categories',
                'icon' => 'fa fa-tasks',
                'menu_id' => 4
            ],
            [
                'name' => 'Units',
                'slug' => 'units',
                'icon' => 'fa fa-sort',
                'menu_id' => 4
            ],
            [
                'name' => 'Admin Setting',
                'slug' => 'admin-setting',
                'icon' => 'fa fa-tasks',
                'menu_id' => 100
            ],

        ]);

        \Illuminate\Database\Eloquent\Model::reguard();
        DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
