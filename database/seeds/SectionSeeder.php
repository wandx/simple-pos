<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("sections")->truncate();
        DB::table("sections")->insert([
            [
                "key" => "setting.adminlabel",
                "value" => "wandxcp"
            ],
            [
                "key" => "setting.admintitle",
                "value" => "Administrator Panel"
            ],
            [
                "key" => "setting.adminskin",
                "value" => "skin-blue"
            ],
            [
                "key" => "setting.adminfooterleft",
                "value" => "Copyright © 2016 Company. All rights reserved."
            ],
            [
                "key" => "setting.adminfooterright",
                "value" => "Anything you want"
            ],
            [
                "key" => "setting.adminloginbg",
                "value" => ""
            ]

        ]);
    }
}
