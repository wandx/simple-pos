<?php

use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("units")->truncate();
        DB::table("units")->insert([
            [
                "name" => "satuan",
                "amount" => 1
            ],
            [
                "name" => "lusin",
                "amount" => 12
            ]
        ]);
    }
}
