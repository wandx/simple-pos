<?php

use Illuminate\Database\Seeder;

class StockActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table("stock_activities")->truncate();
        DB::table("stock_activities")->insert([
            [
                "name" => "add stock",
                "slug" => "add-stock"
            ],
            [
                "name" => "reduce stock from transaction",
                "slug" => "reduce-stock-from-transaction"
            ]
        ]);
    }
}
