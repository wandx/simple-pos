<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->truncate();
        DB::table('admins')->insert([
            'name' => 'Wandy Purnomo',
            'username' => 'wandypurnomo',
            'email' => 'wandypurnomo92@gmail.com',
            'password' => bcrypt('wandx54'),
            'last_login' => \Carbon\Carbon::now(),
            'avatar' => 'http://placehold.it/300x300',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
