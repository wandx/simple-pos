<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("categories")->truncate();
        DB::table("categories")->insert([
            [
                "name" => "Alat Tulis",
                "slug" => "alat-tulis",
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                "name" => "Buku Tulis",
                "slug" => "buku-tulis",
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ],
            [
                "name" => "Buku Gambar",
                "slug" => "buku-gambar",
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]
        ]);
    }
}
