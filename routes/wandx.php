<?php

Route::group(['namespace'=>'Auth'],function(){
    Route::get('/',['middleware'=>'guest:admin','uses'=>'LoginCont@index'])->name('adm.login');
    Route::post('login','LoginCont@doLogin')->name('adm.login.post');
    Route::get('roles',['middleware'=>'auth:admin','uses'=>'LoginCont@roles'])->name('adm.select_role');
    Route::post('roles',['middleware'=>'auth:admin','uses'=>'LoginCont@rolesPost'])->name('adm.select_role.post');
});

Route::group(['middleware'=>['auth:admin','hasmanyrole','last_activity']],function(){
    // Logout
    Route::get('logout','Auth\LoginCont@logout')->name('adm.logout');
    // Dashboard
    Route::group(['namespace'=>'Dashboard','prefix'=>'dashboard','middleware'=>'access:dashboard'],function(){
        Route::get('/','DashboardCont@index')->name('adm.dashboard');
    });

    // User Manager
    Route::group(['namespace'=>'Usermanager','prefix'=>'usermanager','middleware'=>'access:usermanager'],function(){
        Route::get('lists','UserCont@lists')->name('adm.usermanager.lists');
        Route::post('store','UserCont@store')->name('adm.usermanager.store');
        Route::get('destroy/{id}','UserCont@destroy');
        Route::post('update/{id}','UserCont@update');
        Route::get('userdata/{id}',function($id){
            return (new \App\Models\Admin())->newQuery()->where('id',$id)->with('roles')->first();
        });

        Route::group(['prefix'=>'roles'],function(){
            Route::get('/','RoleCont@index')->name('adm.roles');
            Route::get('menu_ids/{role_id}','RoleCont@menuIds');
            Route::post('update_menu','RoleCont@updateMenu')->name('adm.update_menu');
            Route::get('remove_access/{menu_id}/{role_id}','RoleCont@removeAccess');
            Route::post('/','RoleCont@store_role')->name('adm.roles.store');
            Route::get("/{id}","RoleCont@delete_role")->name("adm.roles.delete");
        });
    });

    // Setting
    Route::group(['prefix'=>'setting','namespace'=>'Setting','as'=>'setting.',"middleware"=>"access:setting"],function(){
        Route::group(['prefix'=>'admin-setting','as'=>'admin-setting.'],function(){
            Route::get("/","AdminSetting@index")->name("index");
            Route::post("/","AdminSetting@save_setting")->name("save_setting");
            Route::get("rm-lbg","AdminSetting@removeLoginBg")->name("rm-lbg");
        });

        Route::group(["prefix"=>"print","as"=>"print."],function(){
            Route::get("/","PrintCont@index");
        });
    });

    // Products
    Route::group(["prefix"=>"product","namespace"=>"Product","as"=>"product.","middleware"=>"access:product"],function(){
        Route::get("/","ProductCont@index")->name("index");
        Route::get("data","ProductCont@data")->name("data");
        Route::get("add","ProductCont@add")->name("add");
        Route::post("add","ProductCont@store")->name("store");
        Route::get("view/{id}","ProductCont@view_product")->name("view");
        Route::post("view/{id}","ProductCont@update")->name("update");
        Route::post("add_stock","ProductCont@add_stock")->name("add_stock");
    });

    // POS
    Route::group(["prefix"=>"pos","namespace"=>"Pos","as"=>"pos.","middleware"=>"access:pos"],function(){
        Route::get("/","PosCont@index")->name("index");
        Route::get("data","PosCont@data")->name("data");
        Route::get("add_cart/{id?}/{amount?}","PosCont@add_cart")->name("add_cart");
        Route::get("delete_cart/{id?}","PosCont@delete_cart")->name("delete_cart");
        Route::get("redraw_cart","PosCont@redraw_carts")->name("redraw_carts");
        Route::get("clear_carts","PosCont@clear_carts")->name("clear_carts");
        Route::post("checkout","PosCont@checkout")->name("checkout");
    });

    Route::group(["prefix"=>"transaction","namespace"=>"Transaction","as"=>"transaction.","middleware"=>"access:transaction"],function(){
        Route::get("/","TransCont@index")->name("index");
        Route::get("data","TransCont@data")->name("data");
        Route::get("detail/{id}","TransCont@detail")->name("detail");
    });

    // Misc
    Route::group(["prefix"=>"misc","namespace"=>"Misc","as"=>"misc.","middleware"=>"access:misc"],function(){
        Route::group(["prefix"=>"brands","as"=>"brands."],function(){
            Route::get("/","BrandCont@index")->name("index");
            Route::get("data","BrandCont@data")->name("data");
            Route::post("store","BrandCont@store")->name("store");
            Route::post("update","BrandCont@update")->name("update");
            Route::get("destroy/{id?}","BrandCont@destroy")->name("destroy");
        });

        Route::group(["prefix"=>"categories","as"=>"categories."],function(){
            Route::get("/","CategoryCont@index")->name("index");
            Route::get("data","CategoryCont@data")->name("data");
            Route::post("store","CategoryCont@store")->name("store");
            Route::post("update","CategoryCont@update")->name("update");
            Route::get("destroy/{id?}","CategoryCont@destroy")->name("destroy");
        });

        Route::group(["prefix"=>"units","as"=>"units."],function(){
            Route::get("/","UnitCont@index")->name("index");
            Route::get("data","UnitCont@data")->name("data");
            Route::post("store","UnitCont@store")->name("store");
            Route::post("update","UnitCont@update")->name("update");
            Route::get("destroy/{id?}","UnitCont@destroy")->name("destroy");
        });
    });

    // Asset
    Route::group(["prefix"=>"asset","namespace"=>"Asset","as"=>"asset.","middleware"=>"access:asset"],function(){
        Route::get("/","CashCont@index")->name("index");
        Route::post("balance","CashCont@balance")->name("balance");
        Route::get("remove_record/{id}","CashCont@remove_record")->name("remove_record");
        Route::get("revenue_claim/{amount}","CashCont@revenue_claim")->name("revenue_claim");
        Route::get("move_to_cash/{amount}","CashCont@move_to_cash")->name("move_to_cash");
    });

    // Report
    Route::group(["prefix"=>"report","namespace"=>"Report","as"=>"report.","middleware"=>"access:report"],function(){
        Route::get("/","ReportCont@index")->name("index");
        Route::get("print","ReportCont@print_out")->name("print");
    });
});