<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("/backoffice");
});

Route::get("test",function(){
    $t = new \App\Models\TransactionDetail();

    $t = $t->newQuery()
            ->select(DB::raw("DATE(created_at) as date"),DB::raw("sum(grand_total) as gt"))
            ->whereMonth("created_at","=",\Carbon\Carbon::today()->format("m"))
            ->whereYear("created_at","=",\Carbon\Carbon::today()->format("Y"))
            ->groupBy("date")
            ->orderBy("date","asc")
            ->get();

    $label = [];
    $data = [];
    foreach ($t as $tt){
        $label[] = \Carbon\Carbon::parse($tt->date)->format("d");
    }

    foreach ($t as $ttt){
        $data[] = $ttt->gt;
    }

    return [
        "label" => $label,
        "quantity" => $data
    ];
});
